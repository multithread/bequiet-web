﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBeacon.Models
{
    public class UpdateViewModel
    {
        public Content c { get; set; }
        public IEnumerable<Beacon> beacons { get; set; }
    }
}