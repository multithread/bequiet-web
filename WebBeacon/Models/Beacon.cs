﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBeacon.Models
{
    public class Beacon
    {
        
        public int Id { get; set; }
        public string Major{ get; set; }
        public string Name { get; set; }
        public ICollection<Content> Contents { get; set; }
        public int UserId { get; set; }

    }
}