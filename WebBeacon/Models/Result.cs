﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBeacon.Models
{
    public class Result
    {
        public string value { get; set; }
        public int UserId { get; set; }
    }
}