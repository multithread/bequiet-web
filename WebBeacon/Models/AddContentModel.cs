﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBeacon.Models
{
    public class AddContentModel
    {
        public Content Content { get; set; }
        public Beacon Beacon { get; set; }
    }
}