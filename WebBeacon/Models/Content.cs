﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBeacon.Models
{
    public class Content
    {
        public int Id { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string InstructorName { get; set; }
        public string CourseName { get; set; }
        public string Description { get; set; }
        public int BeaconId { get; set; }
    }
}