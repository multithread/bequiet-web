﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebBeacon.Models
{
    public class BeaconContext:DbContext
    {
        
        public DbSet<User> Users { get; set; }
        public DbSet<Beacon> Beacons { get; set; }
        public DbSet<Content> Contents { get; set; }
    }
}
