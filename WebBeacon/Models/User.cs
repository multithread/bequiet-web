﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebBeacon.Models
{
    public class User
    {
        public User()
        {
            Beacons = new List<Beacon>();
        }
        public int Id { get; set; }
        public string UserEmail { get; set; }
        public string Password { get; set; }
        public virtual ICollection<Beacon> Beacons { get; set; }
    }
}