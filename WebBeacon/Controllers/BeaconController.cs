﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBeacon.Models;

namespace WebBeacon.Controllers
{
    [Authorize]
    public class BeaconController : Controller
    {
        //
        // GET: /Beacon/
        private BeaconContext db=new BeaconContext();

        public ActionResult AddContent()
        {
            User us = db.Users.Where(u => u.UserEmail == User.Identity.Name).FirstOrDefault();
            List<SelectListItem> beacons = new List<SelectListItem>();
            foreach (var item in us.Beacons)
            {
                beacons.Add(new SelectListItem { Text = item.Name, Value = item.Id.ToString() });

            }
            ViewBag.beacons = beacons;
            return View();
        }

        public ActionResult Contents()
        {
            List<AddContentModel> addcontents=new List<AddContentModel>();
            AddContentModel ad;
            User us = db.Users.Where(u => u.UserEmail == User.Identity.Name).FirstOrDefault();
            List<Beacon> beacons = db.Beacons.Where(b => b.UserId == us.Id).ToList();
            foreach (var item in beacons)
            {
                foreach (var content in db.Contents.Where(c=>c.BeaconId==item.Id))
                {
                    ad = new AddContentModel();
                    ad.Beacon = item;
                    ad.Content = content;
                    addcontents.Add(ad);
                    
                }
            }
             
            
            return View(addcontents);
        }

        [HttpPost]
        public ActionResult AddContent(Content newContent,int beacon)
        {
            Beacon b = db.Beacons.Find(beacon);
            newContent.BeaconId = beacon;
            db.Contents.Add(newContent);
            db.SaveChanges();
            return RedirectToAction("Contents");
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Content c = db.Contents.Find(id);
            db.Contents.Remove(c);
            db.SaveChanges();

            return RedirectToAction("Contents");
        }

        public ActionResult Update(int id)
        {
            User us = db.Users.Where(u => u.UserEmail == User.Identity.Name).FirstOrDefault();
            List<Beacon> beacons = us.Beacons.ToList();
            UpdateViewModel up = new UpdateViewModel();
            up.c=db.Contents.Find(id);
            up.beacons = beacons;
            return View(up);
        }

        [HttpPost]
        public ActionResult Update(Content updatedContent)
        {
            db.Entry(updatedContent).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Contents");
        }
        public string saat()
        {
            return DateTime.Now.AddHours(2).ToString();
        }

    }
}
