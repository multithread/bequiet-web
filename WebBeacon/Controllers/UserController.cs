﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebBeacon.Models;

namespace WebBeacon.Controllers
{
    public class UserController : ApiController
    {
        private BeaconContext db = new BeaconContext();

        public IEnumerable<Beacon> Get(int UserId)
        {
            List<Beacon> beacons=db.Beacons.Where(x => x.UserId == UserId).ToList();
            return beacons;
        }
    }
}
