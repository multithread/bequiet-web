﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebBeacon.Models;

namespace WebBeacon.Controllers
{

    [AllowAnonymous]
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        private BeaconContext db = new BeaconContext();

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {

            var count = db.Users.Where(u => u.UserEmail == user.UserEmail && u.Password == user.Password).Count();

            if (count == 0)
            {
                ViewBag.Msg = "Invalid User";
                return View();
            }
            else
            {
                FormsAuthentication.SetAuthCookie(user.UserEmail, false);
                return RedirectToAction("Contents", "Beacon");
            }
        }

        [Authorize]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        public ActionResult au(string email,string password)
        {
            User u=new User();
            u.UserEmail=email;
            u.Password=password;
            db.Users.Add(u);
            db.SaveChanges();

            return Json("succes", JsonRequestBehavior.AllowGet);
        }

    }
}
