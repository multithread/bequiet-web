﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebBeacon.Models;

namespace WebBeacon.Controllers
{
    public class ContentApiController : ApiController
    {
        private BeaconContext db = new BeaconContext();

        public IEnumerable<Content> Get()
        {
            return db.Contents.ToList();
        }

        public Content Gets(int id)
        {
            Content contents;
            DateTime d=DateTime.Now;
            d = d.AddHours(2);

            try
            {
                contents = db.Contents.Where(x => x.BeaconId == id).Where(x => x.StartDateTime < d).Where(x => x.EndDateTime > d).First();
                return contents;
            }
            catch (Exception e)
            {
                return null;
            }
            
           
        }

        public HttpResponseMessage Post([FromBody]Content newContent)
        {
            try
            {
                db.Contents.Add(newContent);
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
        public HttpResponseMessage Put([FromBody]Content updatedContent)
        {
            try
            {
                db.Entry(updatedContent).State = EntityState.Modified;
                db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
        }
    }
}

