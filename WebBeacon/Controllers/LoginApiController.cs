﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebBeacon.Models;

namespace WebBeacon.Controllers
{
    public class LoginApiController : ApiController
    {
        private BeaconContext db = new BeaconContext();

        public Result Get(string Email,string Password)
        {
            Result r = new Result();
            try
            {
                var count = db.Users.Where(u => u.UserEmail ==Email && u.Password ==Password).Count();

                if (count == 0)
                {
                    r.value = "No";
                    r.UserId = -1;
                    return r;
                }
                else
                {

                    r.value = "Ok";
                    r.UserId = db.Users.Where(x => x.UserEmail == Email && x.Password == Password).First().Id;
                    return r;
                }
            }
            catch
            {
                r.value = "Hata";
                return r;
            }
        }
    }
}
